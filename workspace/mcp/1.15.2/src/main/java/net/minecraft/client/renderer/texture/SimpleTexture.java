package net.minecraft.client.renderer.texture;

import com.mojang.blaze3d.systems.RenderSystem;
import java.io.Closeable;
import java.io.IOException;
import javax.annotation.Nullable;
import net.minecraft.client.resources.data.TextureMetadataSection;
import net.minecraft.resources.IResource;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@OnlyIn(Dist.CLIENT)
public class SimpleTexture extends Texture {
   private static final Logger LOGGER = LogManager.getLogger();
   protected final ResourceLocation textureLocation;

   public SimpleTexture(ResourceLocation textureResourceLocation) {
      this.textureLocation = textureResourceLocation;
   }

   public void loadTexture(IResourceManager manager) throws IOException {
      SimpleTexture.TextureData simpletexture$texturedata = this.getTextureData(manager);
      simpletexture$texturedata.checkException();
      TextureMetadataSection texturemetadatasection = simpletexture$texturedata.getMetadata();
      boolean flag;
      boolean flag1;
      if (texturemetadatasection != null) {
         flag = texturemetadatasection.getTextureBlur();
         flag1 = texturemetadatasection.getTextureClamp();
      } else {
         flag = false;
         flag1 = false;
      }

      NativeImage nativeimage = simpletexture$texturedata.getNativeImage();
      if (!RenderSystem.isOnRenderThreadOrInit()) {
         RenderSystem.recordRenderCall(() -> {
            this.func_229207_a_(nativeimage, flag, flag1);
         });
      } else {
         this.func_229207_a_(nativeimage, flag, flag1);
      }

   }

   private void func_229207_a_(NativeImage p_229207_1_, boolean p_229207_2_, boolean p_229207_3_) {
      TextureUtil.func_225681_a_(this.getGlTextureId(), 0, p_229207_1_.getWidth(), p_229207_1_.getHeight());
      p_229207_1_.func_227789_a_(0, 0, 0, 0, 0, p_229207_1_.getWidth(), p_229207_1_.getHeight(), p_229207_2_, p_229207_3_, false, true);
   }

   protected SimpleTexture.TextureData getTextureData(IResourceManager resourceManager) {
      return SimpleTexture.TextureData.getTextureData(resourceManager, this.textureLocation);
   }

   @OnlyIn(Dist.CLIENT)
   public static class TextureData implements Closeable {
      @Nullable
      private final TextureMetadataSection metadata;
      @Nullable
      private final NativeImage nativeImage;
      @Nullable
      private final IOException exception;

      public TextureData(IOException p_i50473_1_) {
         this.exception = p_i50473_1_;
         this.metadata = null;
         this.nativeImage = null;
      }

      public TextureData(@Nullable TextureMetadataSection p_i50474_1_, NativeImage p_i50474_2_) {
         this.exception = null;
         this.metadata = p_i50474_1_;
         this.nativeImage = p_i50474_2_;
      }

      public static SimpleTexture.TextureData getTextureData(IResourceManager p_217799_0_, ResourceLocation p_217799_1_) {
         try (IResource iresource = p_217799_0_.getResource(p_217799_1_)) {
            NativeImage nativeimage = NativeImage.read(iresource.getInputStream());
            TextureMetadataSection texturemetadatasection = null;

            try {
               texturemetadatasection = iresource.getMetadata(TextureMetadataSection.SERIALIZER);
            } catch (RuntimeException runtimeexception) {
               SimpleTexture.LOGGER.warn("Failed reading metadata of: {}", p_217799_1_, runtimeexception);
            }

            SimpleTexture.TextureData lvt_6_1_ = new SimpleTexture.TextureData(texturemetadatasection, nativeimage);
            return lvt_6_1_;
         } catch (IOException ioexception) {
            return new SimpleTexture.TextureData(ioexception);
         }
      }

      @Nullable
      public TextureMetadataSection getMetadata() {
         return this.metadata;
      }

      public NativeImage getNativeImage() throws IOException {
         if (this.exception != null) {
            throw this.exception;
         } else {
            return this.nativeImage;
         }
      }

      public void close() {
         if (this.nativeImage != null) {
            this.nativeImage.close();
         }

      }

      public void checkException() throws IOException {
         if (this.exception != null) {
            throw this.exception;
         }
      }
   }
}
