package com.mojang.blaze3d.vertex;

import com.mojang.blaze3d.matrix.MatrixStack;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import net.minecraft.client.renderer.Matrix3f;
import net.minecraft.client.renderer.Matrix4f;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.Vector4f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.math.Vec3i;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.system.MemoryStack;

@OnlyIn(Dist.CLIENT)
public interface IVertexBuilder {
   Logger LOGGER = LogManager.getLogger();

   IVertexBuilder pos(double p_225582_1_, double p_225582_3_, double p_225582_5_);

   IVertexBuilder color(int p_225586_1_, int p_225586_2_, int p_225586_3_, int p_225586_4_);

   IVertexBuilder tex(float p_225583_1_, float p_225583_2_);

   IVertexBuilder func_225585_a_(int p_225585_1_, int p_225585_2_);

   IVertexBuilder lightmap(int p_225587_1_, int p_225587_2_);

   IVertexBuilder func_225584_a_(float p_225584_1_, float p_225584_2_, float p_225584_3_);

   void endVertex();

   default void func_225588_a_(float p_225588_1_, float p_225588_2_, float p_225588_3_, float red, float green, float blue, float alpha, float p_225588_8_, float p_225588_9_, int p_225588_10_, int p_225588_11_, float p_225588_12_, float p_225588_13_, float p_225588_14_) {
      this.pos((double)p_225588_1_, (double)p_225588_2_, (double)p_225588_3_);
      this.color(red, green, blue, alpha);
      this.tex(p_225588_8_, p_225588_9_);
      this.func_227891_b_(p_225588_10_);
      this.lightmap(p_225588_11_);
      this.func_225584_a_(p_225588_12_, p_225588_13_, p_225588_14_);
      this.endVertex();
   }

   default IVertexBuilder color(float p_227885_1_, float p_227885_2_, float p_227885_3_, float p_227885_4_) {
      return this.color((int)(p_227885_1_ * 255.0F), (int)(p_227885_2_ * 255.0F), (int)(p_227885_3_ * 255.0F), (int)(p_227885_4_ * 255.0F));
   }

   default IVertexBuilder lightmap(int packedLight) {
      return this.lightmap(packedLight & '\uffff', packedLight >> 16 & '\uffff');
   }

   default IVertexBuilder func_227891_b_(int packedLight) {
      return this.func_225585_a_(packedLight & '\uffff', packedLight >> 16 & '\uffff');
   }

   default void addVertexData(MatrixStack.Entry p_227889_1_, BakedQuad p_227889_2_, float p_227889_3_, float p_227889_4_, float p_227889_5_, int p_227889_6_, int p_227889_7_) {
      this.addVertexData(p_227889_1_, p_227889_2_, new float[]{1.0F, 1.0F, 1.0F, 1.0F}, p_227889_3_, p_227889_4_, p_227889_5_, new int[]{p_227889_6_, p_227889_6_, p_227889_6_, p_227889_6_}, p_227889_7_, false);
   }

   default void addVertexData(MatrixStack.Entry matrixStackIn, BakedQuad quadIn, float[] p_227890_3_, float red, float green, float blue, int[] lightmapCoords, int overlayColor, boolean readExistingColor) {
      int[] aint = quadIn.getVertexData();
      Vec3i vec3i = quadIn.getFace().getDirectionVec();
      Vector3f vector3f = new Vector3f((float)vec3i.getX(), (float)vec3i.getY(), (float)vec3i.getZ());
      Matrix4f matrix4f = matrixStackIn.getPositionMatrix();
      vector3f.func_229188_a_(matrixStackIn.getNormalMatrix());
      int i = 8;
      int j = aint.length / 8;

      try (MemoryStack memorystack = MemoryStack.stackPush()) {
         ByteBuffer bytebuffer = memorystack.malloc(DefaultVertexFormats.BLOCK.getSize());
         IntBuffer intbuffer = bytebuffer.asIntBuffer();

         for(int k = 0; k < j; ++k) {
            intbuffer.clear();
            intbuffer.put(aint, k * 8, 8);
            float f = bytebuffer.getFloat(0);
            float f1 = bytebuffer.getFloat(4);
            float f2 = bytebuffer.getFloat(8);
            float f3;
            float f4;
            float f5;
            if (readExistingColor) {
               float f6 = (float)(bytebuffer.get(12) & 255) / 255.0F;
               float f7 = (float)(bytebuffer.get(13) & 255) / 255.0F;
               float f8 = (float)(bytebuffer.get(14) & 255) / 255.0F;
               f3 = f6 * p_227890_3_[k] * red;
               f4 = f7 * p_227890_3_[k] * green;
               f5 = f8 * p_227890_3_[k] * blue;
            } else {
               f3 = p_227890_3_[k] * red;
               f4 = p_227890_3_[k] * green;
               f5 = p_227890_3_[k] * blue;
            }

            int l = lightmapCoords[k];
            float f9 = bytebuffer.getFloat(16);
            float f10 = bytebuffer.getFloat(20);
            Vector4f vector4f = new Vector4f(f, f1, f2, 1.0F);
            vector4f.func_229372_a_(matrix4f);
            this.func_225588_a_(vector4f.getX(), vector4f.getY(), vector4f.getZ(), f3, f4, f5, 1.0F, f9, f10, overlayColor, l, vector3f.getX(), vector3f.getY(), vector3f.getZ());
         }
      }

   }

   default IVertexBuilder pos(Matrix4f p_227888_1_, float p_227888_2_, float p_227888_3_, float p_227888_4_) {
      Vector4f vector4f = new Vector4f(p_227888_2_, p_227888_3_, p_227888_4_, 1.0F);
      vector4f.func_229372_a_(p_227888_1_);
      return this.pos((double)vector4f.getX(), (double)vector4f.getY(), (double)vector4f.getZ());
   }

   default IVertexBuilder func_227887_a_(Matrix3f p_227887_1_, float p_227887_2_, float p_227887_3_, float p_227887_4_) {
      Vector3f vector3f = new Vector3f(p_227887_2_, p_227887_3_, p_227887_4_);
      vector3f.func_229188_a_(p_227887_1_);
      return this.func_225584_a_(vector3f.getX(), vector3f.getY(), vector3f.getZ());
   }
}
