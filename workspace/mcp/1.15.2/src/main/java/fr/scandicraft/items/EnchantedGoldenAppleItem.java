package fr.scandicraft.items;

import fr.scandicraft.Config;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class EnchantedGoldenAppleItem extends Item {
	public EnchantedGoldenAppleItem(Item.Properties p_i50045_1_) {
		super(p_i50045_1_);
	}

	@Override
	public ItemStack onItemUseFinish(ItemStack stack, World worldIn, LivingEntity entityLiving) {
		PlayerEntity player = (PlayerEntity) entityLiving;
		player.getCooldownTracker().setCooldown(this, Config.ENCHANTEDGOLDEN_APPLE_COOLDOWN);

		return super.onItemUseFinish(stack, worldIn, entityLiving);
	}

	public boolean hasEffect(ItemStack stack) {
		return true;
	}
}
