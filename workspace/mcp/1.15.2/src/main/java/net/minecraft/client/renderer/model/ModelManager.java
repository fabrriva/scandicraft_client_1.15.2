package net.minecraft.client.renderer.model;

import it.unimi.dsi.fastutil.objects.Object2IntMap;
import java.util.Map;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.BlockModelShapes;
import net.minecraft.client.renderer.color.BlockColors;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.SpriteMap;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.resources.ReloadListener;
import net.minecraft.fluid.IFluidState;
import net.minecraft.profiler.IProfiler;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ModelManager extends ReloadListener<ModelBakery> implements AutoCloseable {
   private Map<ResourceLocation, IBakedModel> modelRegistry;
   private SpriteMap atlases;
   private final BlockModelShapes modelProvider;
   private final TextureManager textureManager;
   private final BlockColors blockColors;
   private int maxMipmapLevel;
   private IBakedModel defaultModel;
   private Object2IntMap<BlockState> stateModelIds;

   public ModelManager(TextureManager p_i226057_1_, BlockColors p_i226057_2_, int p_i226057_3_) {
      this.textureManager = p_i226057_1_;
      this.blockColors = p_i226057_2_;
      this.maxMipmapLevel = p_i226057_3_;
      this.modelProvider = new BlockModelShapes(this);
   }

   public IBakedModel getModel(ModelResourceLocation modelLocation) {
      return this.modelRegistry.getOrDefault(modelLocation, this.defaultModel);
   }

   public IBakedModel getMissingModel() {
      return this.defaultModel;
   }

   public BlockModelShapes getBlockModelShapes() {
      return this.modelProvider;
   }

   protected ModelBakery prepare(IResourceManager resourceManagerIn, IProfiler profilerIn) {
      profilerIn.startTick();
      ModelBakery modelbakery = new ModelBakery(resourceManagerIn, this.blockColors, profilerIn, this.maxMipmapLevel);
      profilerIn.endTick();
      return modelbakery;
   }

   protected void apply(ModelBakery splashList, IResourceManager resourceManagerIn, IProfiler profilerIn) {
      profilerIn.startTick();
      profilerIn.startSection("upload");
      if (this.atlases != null) {
         this.atlases.close();
      }

      this.atlases = splashList.func_229333_a_(this.textureManager, profilerIn);
      this.modelRegistry = splashList.func_217846_a();
      this.stateModelIds = splashList.func_225354_b();
      this.defaultModel = this.modelRegistry.get(ModelBakery.MODEL_MISSING);
      profilerIn.endStartSection("cache");
      this.modelProvider.reloadModels();
      profilerIn.endSection();
      profilerIn.endTick();
   }

   public boolean func_224742_a(BlockState p_224742_1_, BlockState p_224742_2_) {
      if (p_224742_1_ == p_224742_2_) {
         return false;
      } else {
         int i = this.stateModelIds.getInt(p_224742_1_);
         if (i != -1) {
            int j = this.stateModelIds.getInt(p_224742_2_);
            if (i == j) {
               IFluidState ifluidstate = p_224742_1_.getFluidState();
               IFluidState ifluidstate1 = p_224742_2_.getFluidState();
               return ifluidstate != ifluidstate1;
            }
         }

         return true;
      }
   }

   public AtlasTexture func_229356_a_(ResourceLocation p_229356_1_) {
      return this.atlases.func_229152_a_(p_229356_1_);
   }

   public void close() {
      this.atlases.close();
   }

   public void func_229355_a_(int p_229355_1_) {
      this.maxMipmapLevel = p_229355_1_;
   }
}
