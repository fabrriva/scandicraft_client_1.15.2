package fr.scandicraft;

import java.util.Arrays;
import java.util.List;

import net.minecraft.item.Item;
import net.minecraft.item.Items;

public final class Config {

	/* Variables */
	public static String SERVER_NAME = "ScandiCraft";
	public static String SERVER_VERSION = "Reborn v1";
	public static String COPYRIGHT = "Copyright Mojang AB. Do not distribute!";
	public static int ENDERPEARL_COOLDOWN = 20;	//20 ticks = 1 secondes
	public static int ENCHANTEDGOLDEN_APPLE_COOLDOWN = 600;	//600 ticks = 30 secondes
	public static int GOLDEN_APPLE_COOLDOWN = 300;	//300 ticks = 15 secondes
	public static List<Item> allowedItemsOffHand = Arrays.asList(Items.TORCH);
	public static final int SLOT_OFFHAND = 40; //correspond au slot de la OFFHAND
	
}