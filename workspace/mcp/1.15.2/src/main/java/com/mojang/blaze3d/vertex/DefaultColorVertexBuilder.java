package com.mojang.blaze3d.vertex;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public abstract class DefaultColorVertexBuilder implements IVertexBuilder {
   protected boolean defaultColor = false;
   protected int defaultRed = 255;
   protected int defaultGreen = 255;
   protected int defaultBlue = 255;
   protected int defaultAlpha = 255;

   public void func_225611_b_(int p_225611_1_, int p_225611_2_, int p_225611_3_, int p_225611_4_) {
      this.defaultRed = p_225611_1_;
      this.defaultGreen = p_225611_2_;
      this.defaultBlue = p_225611_3_;
      this.defaultAlpha = p_225611_4_;
      this.defaultColor = true;
   }
}
