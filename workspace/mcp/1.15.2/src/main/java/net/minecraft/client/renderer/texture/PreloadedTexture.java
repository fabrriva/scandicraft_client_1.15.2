package net.minecraft.client.renderer.texture;

import com.mojang.blaze3d.systems.RenderSystem;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import javax.annotation.Nullable;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Util;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class PreloadedTexture extends SimpleTexture {
   @Nullable
   private CompletableFuture<SimpleTexture.TextureData> textureDataFuture;

   public PreloadedTexture(IResourceManager p_i50911_1_, ResourceLocation p_i50911_2_, Executor p_i50911_3_) {
      super(p_i50911_2_);
      this.textureDataFuture = CompletableFuture.supplyAsync(() -> {
         return SimpleTexture.TextureData.getTextureData(p_i50911_1_, p_i50911_2_);
      }, p_i50911_3_);
   }

   protected SimpleTexture.TextureData getTextureData(IResourceManager resourceManager) {
      if (this.textureDataFuture != null) {
         SimpleTexture.TextureData simpletexture$texturedata = this.textureDataFuture.join();
         this.textureDataFuture = null;
         return simpletexture$texturedata;
      } else {
         return SimpleTexture.TextureData.getTextureData(resourceManager, this.textureLocation);
      }
   }

   public CompletableFuture<Void> getCompletableFuture() {
      return this.textureDataFuture == null ? CompletableFuture.completedFuture((Void)null) : this.textureDataFuture.thenApply((p_215247_0_) -> {
         return null;
      });
   }

   public void loadTexture(TextureManager p_215244_1_, IResourceManager p_215244_2_, ResourceLocation p_215244_3_, Executor p_215244_4_) {
      this.textureDataFuture = CompletableFuture.supplyAsync(() -> {
         return SimpleTexture.TextureData.getTextureData(p_215244_2_, this.textureLocation);
      }, Util.getServerExecutor());
      this.textureDataFuture.thenRunAsync(() -> {
         p_215244_1_.loadTexture(this.textureLocation, this);
      }, func_229205_a_(p_215244_4_));
   }

   private static Executor func_229205_a_(Executor p_229205_0_) {
      return (p_229206_1_) -> {
         p_229205_0_.execute(() -> {
            RenderSystem.recordRenderCall(p_229206_1_::run);
         });
      };
   }
}
