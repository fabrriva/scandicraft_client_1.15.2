package net.minecraft.client.renderer;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.primitives.Floats;
import com.mojang.blaze3d.vertex.DefaultColorVertexBuilder;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.vertex.IVertexConsumer;
import com.mojang.datafixers.util.Pair;
import it.unimi.dsi.fastutil.ints.IntArrays;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.BitSet;
import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.client.renderer.vertex.VertexFormatElement;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@OnlyIn(Dist.CLIENT)
public class BufferBuilder extends DefaultColorVertexBuilder implements IVertexConsumer {
   private static final Logger LOGGER = LogManager.getLogger();
   private ByteBuffer byteBuffer;
   private final List<BufferBuilder.DrawState> drawStates = Lists.newArrayList();
   private int drawStateIndex = 0;
   private int renderedBytes = 0;
   private int nextElementBytes = 0;
   private int uploadedBytes = 0;
   private int vertexCount;
   @Nullable
   private VertexFormatElement vertexFormatElement;
   private int vertexFormatIndex;
   private int drawMode;
   private VertexFormat vertexFormat;
   private boolean fastFormat;
   private boolean fullFormat;
   private boolean isDrawing;

   public BufferBuilder(int bufferSizeIn) {
      this.byteBuffer = GLAllocation.createDirectByteBuffer(bufferSizeIn * 4);
   }

   protected void growBuffer() {
      this.growBuffer(this.vertexFormat.getSize());
   }

   private void growBuffer(int increaseAmount) {
      if (this.nextElementBytes + increaseAmount > this.byteBuffer.capacity()) {
         int i = this.byteBuffer.capacity();
         int j = i + roundUpPositive(increaseAmount);
         LOGGER.debug("Needed to grow BufferBuilder buffer: Old size {} bytes, new size {} bytes.", i, j);
         ByteBuffer bytebuffer = GLAllocation.createDirectByteBuffer(j);
         this.byteBuffer.position(0);
         bytebuffer.put(this.byteBuffer);
         bytebuffer.rewind();
         this.byteBuffer = bytebuffer;
      }
   }

   private static int roundUpPositive(int p_216566_0_) {
      int i = 2097152;
      if (p_216566_0_ == 0) {
         return i;
      } else {
         if (p_216566_0_ < 0) {
            i *= -1;
         }

         int j = p_216566_0_ % i;
         return j == 0 ? p_216566_0_ : p_216566_0_ + i - j;
      }
   }

   public void sortVertexData(float cameraX, float cameraY, float cameraZ) {
      this.byteBuffer.clear();
      FloatBuffer floatbuffer = this.byteBuffer.asFloatBuffer();
      int i = this.vertexCount / 4;
      float[] afloat = new float[i];

      for(int j = 0; j < i; ++j) {
         afloat[j] = getDistanceSq(floatbuffer, cameraX, cameraY, cameraZ, this.vertexFormat.getIntegerSize(), this.renderedBytes / 4 + j * this.vertexFormat.getSize());
      }

      int[] aint = new int[i];

      for(int k = 0; k < aint.length; aint[k] = k++) {
         ;
      }

      IntArrays.mergeSort(aint, (p_227830_1_, p_227830_2_) -> {
         return Floats.compare(afloat[p_227830_2_], afloat[p_227830_1_]);
      });
      BitSet bitset = new BitSet();
      FloatBuffer floatbuffer1 = GLAllocation.createDirectFloatBuffer(this.vertexFormat.getIntegerSize() * 4);

      for(int l = bitset.nextClearBit(0); l < aint.length; l = bitset.nextClearBit(l + 1)) {
         int i1 = aint[l];
         if (i1 != l) {
            this.func_227829_a_(floatbuffer, i1);
            floatbuffer1.clear();
            floatbuffer1.put(floatbuffer);
            int j1 = i1;

            for(int k1 = aint[i1]; j1 != l; k1 = aint[k1]) {
               this.func_227829_a_(floatbuffer, k1);
               FloatBuffer floatbuffer2 = floatbuffer.slice();
               this.func_227829_a_(floatbuffer, j1);
               floatbuffer.put(floatbuffer2);
               bitset.set(j1);
               j1 = k1;
            }

            this.func_227829_a_(floatbuffer, l);
            floatbuffer1.flip();
            floatbuffer.put(floatbuffer1);
         }

         bitset.set(l);
      }

   }

   private void func_227829_a_(FloatBuffer p_227829_1_, int p_227829_2_) {
      int i = this.vertexFormat.getIntegerSize() * 4;
      p_227829_1_.limit(this.renderedBytes / 4 + (p_227829_2_ + 1) * i);
      p_227829_1_.position(this.renderedBytes / 4 + p_227829_2_ * i);
   }

   public BufferBuilder.State getVertexState() {
      this.byteBuffer.limit(this.nextElementBytes);
      this.byteBuffer.position(this.renderedBytes);
      ByteBuffer bytebuffer = ByteBuffer.allocate(this.vertexCount * this.vertexFormat.getSize());
      bytebuffer.put(this.byteBuffer);
      this.byteBuffer.clear();
      return new BufferBuilder.State(bytebuffer, this.vertexFormat);
   }

   private static float getDistanceSq(FloatBuffer floatBufferIn, float x, float y, float z, int integerSize, int offset) {
      float f = floatBufferIn.get(offset + integerSize * 0 + 0);
      float f1 = floatBufferIn.get(offset + integerSize * 0 + 1);
      float f2 = floatBufferIn.get(offset + integerSize * 0 + 2);
      float f3 = floatBufferIn.get(offset + integerSize * 1 + 0);
      float f4 = floatBufferIn.get(offset + integerSize * 1 + 1);
      float f5 = floatBufferIn.get(offset + integerSize * 1 + 2);
      float f6 = floatBufferIn.get(offset + integerSize * 2 + 0);
      float f7 = floatBufferIn.get(offset + integerSize * 2 + 1);
      float f8 = floatBufferIn.get(offset + integerSize * 2 + 2);
      float f9 = floatBufferIn.get(offset + integerSize * 3 + 0);
      float f10 = floatBufferIn.get(offset + integerSize * 3 + 1);
      float f11 = floatBufferIn.get(offset + integerSize * 3 + 2);
      float f12 = (f + f3 + f6 + f9) * 0.25F - x;
      float f13 = (f1 + f4 + f7 + f10) * 0.25F - y;
      float f14 = (f2 + f5 + f8 + f11) * 0.25F - z;
      return f12 * f12 + f13 * f13 + f14 * f14;
   }

   public void setVertexState(BufferBuilder.State state) {
      state.stateByteBuffer.clear();
      int i = state.stateByteBuffer.capacity();
      this.growBuffer(i);
      this.byteBuffer.limit(this.byteBuffer.capacity());
      this.byteBuffer.position(this.renderedBytes);
      this.byteBuffer.put(state.stateByteBuffer);
      this.byteBuffer.clear();
      VertexFormat vertexformat = state.stateVertexFormat;
      this.setVertexFormat(vertexformat);
      this.vertexCount = i / vertexformat.getSize();
      this.nextElementBytes = this.renderedBytes + this.vertexCount * vertexformat.getSize();
   }

   public void begin(int glMode, VertexFormat format) {
      if (this.isDrawing) {
         throw new IllegalStateException("Already building!");
      } else {
         this.isDrawing = true;
         this.drawMode = glMode;
         this.setVertexFormat(format);
         this.vertexFormatElement = format.func_227894_c_().get(0);
         this.vertexFormatIndex = 0;
         this.byteBuffer.clear();
      }
   }

   private void setVertexFormat(VertexFormat newFormat) {
      if (this.vertexFormat != newFormat) {
         this.vertexFormat = newFormat;
         boolean flag = newFormat == DefaultVertexFormats.ITEM;
         boolean flag1 = newFormat == DefaultVertexFormats.BLOCK;
         this.fastFormat = flag || flag1;
         this.fullFormat = flag;
      }
   }

   public void finishDrawing() {
      if (!this.isDrawing) {
         throw new IllegalStateException("Not building!");
      } else {
         this.isDrawing = false;
         this.drawStates.add(new BufferBuilder.DrawState(this.vertexFormat, this.vertexCount, this.drawMode));
         this.renderedBytes += this.vertexCount * this.vertexFormat.getSize();
         this.vertexCount = 0;
         this.vertexFormatElement = null;
         this.vertexFormatIndex = 0;
      }
   }

   public void putByte(int p_225589_1_, byte p_225589_2_) {
      this.byteBuffer.put(this.nextElementBytes + p_225589_1_, p_225589_2_);
   }

   public void putShort(int p_225591_1_, short p_225591_2_) {
      this.byteBuffer.putShort(this.nextElementBytes + p_225591_1_, p_225591_2_);
   }

   public void putFloat(int p_225590_1_, float p_225590_2_) {
      this.byteBuffer.putFloat(this.nextElementBytes + p_225590_1_, p_225590_2_);
   }

   public void endVertex() {
      if (this.vertexFormatIndex != 0) {
         throw new IllegalStateException("Not filled all elements of the vertex");
      } else {
         ++this.vertexCount;
         this.growBuffer();
      }
   }

   public void nextVertexFormatIndex() {
      ImmutableList<VertexFormatElement> immutablelist = this.vertexFormat.func_227894_c_();
      this.vertexFormatIndex = (this.vertexFormatIndex + 1) % immutablelist.size();
      this.nextElementBytes += this.vertexFormatElement.getSize();
      VertexFormatElement vertexformatelement = immutablelist.get(this.vertexFormatIndex);
      this.vertexFormatElement = vertexformatelement;
      if (vertexformatelement.getUsage() == VertexFormatElement.Usage.PADDING) {
         this.nextVertexFormatIndex();
      }

      if (this.defaultColor && this.vertexFormatElement.getUsage() == VertexFormatElement.Usage.COLOR) {
         IVertexConsumer.super.color(this.defaultRed, this.defaultGreen, this.defaultBlue, this.defaultAlpha);
      }

   }

   public IVertexBuilder color(int p_225586_1_, int p_225586_2_, int p_225586_3_, int p_225586_4_) {
      if (this.defaultColor) {
         throw new IllegalStateException();
      } else {
         return IVertexConsumer.super.color(p_225586_1_, p_225586_2_, p_225586_3_, p_225586_4_);
      }
   }

   public void func_225588_a_(float p_225588_1_, float p_225588_2_, float p_225588_3_, float red, float green, float blue, float alpha, float p_225588_8_, float p_225588_9_, int p_225588_10_, int p_225588_11_, float p_225588_12_, float p_225588_13_, float p_225588_14_) {
      if (this.defaultColor) {
         throw new IllegalStateException();
      } else if (this.fastFormat) {
         this.putFloat(0, p_225588_1_);
         this.putFloat(4, p_225588_2_);
         this.putFloat(8, p_225588_3_);
         this.putByte(12, (byte)((int)(red * 255.0F)));
         this.putByte(13, (byte)((int)(green * 255.0F)));
         this.putByte(14, (byte)((int)(blue * 255.0F)));
         this.putByte(15, (byte)((int)(alpha * 255.0F)));
         this.putFloat(16, p_225588_8_);
         this.putFloat(20, p_225588_9_);
         int i;
         if (this.fullFormat) {
            this.putShort(24, (short)(p_225588_10_ & '\uffff'));
            this.putShort(26, (short)(p_225588_10_ >> 16 & '\uffff'));
            i = 28;
         } else {
            i = 24;
         }

         this.putShort(i + 0, (short)(p_225588_11_ & '\uffff'));
         this.putShort(i + 2, (short)(p_225588_11_ >> 16 & '\uffff'));
         this.putByte(i + 4, IVertexConsumer.func_227846_a_(p_225588_12_));
         this.putByte(i + 5, IVertexConsumer.func_227846_a_(p_225588_13_));
         this.putByte(i + 6, IVertexConsumer.func_227846_a_(p_225588_14_));
         this.nextElementBytes += i + 8;
         this.endVertex();
      } else {
         super.func_225588_a_(p_225588_1_, p_225588_2_, p_225588_3_, red, green, blue, alpha, p_225588_8_, p_225588_9_, p_225588_10_, p_225588_11_, p_225588_12_, p_225588_13_, p_225588_14_);
      }
   }

   public Pair<BufferBuilder.DrawState, ByteBuffer> getAndResetData() {
      BufferBuilder.DrawState bufferbuilder$drawstate = this.drawStates.get(this.drawStateIndex++);
      this.byteBuffer.position(this.uploadedBytes);
      this.uploadedBytes += bufferbuilder$drawstate.getVertexCount() * bufferbuilder$drawstate.getFormat().getSize();
      this.byteBuffer.limit(this.uploadedBytes);
      if (this.drawStateIndex == this.drawStates.size() && this.vertexCount == 0) {
         this.reset();
      }

      ByteBuffer bytebuffer = this.byteBuffer.slice();
      this.byteBuffer.clear();
      return Pair.of(bufferbuilder$drawstate, bytebuffer);
   }

   public void reset() {
      if (this.renderedBytes != this.uploadedBytes) {
         LOGGER.warn("Bytes mismatch " + this.renderedBytes + " " + this.uploadedBytes);
      }

      this.func_227833_h_();
   }

   public void func_227833_h_() {
      this.renderedBytes = 0;
      this.uploadedBytes = 0;
      this.nextElementBytes = 0;
      this.drawStates.clear();
      this.drawStateIndex = 0;
   }

   public VertexFormatElement getCurrentElement() {
      if (this.vertexFormatElement == null) {
         throw new IllegalStateException("BufferBuilder not started");
      } else {
         return this.vertexFormatElement;
      }
   }

   public boolean isDrawing() {
      return this.isDrawing;
   }

   @OnlyIn(Dist.CLIENT)
   public static final class DrawState {
      private final VertexFormat format;
      private final int vertexCount;
      private final int drawMode;

      private DrawState(VertexFormat formatIn, int vertexCountIn, int drawModeIn) {
         this.format = formatIn;
         this.vertexCount = vertexCountIn;
         this.drawMode = drawModeIn;
      }

      public VertexFormat getFormat() {
         return this.format;
      }

      public int getVertexCount() {
         return this.vertexCount;
      }

      public int getDrawMode() {
         return this.drawMode;
      }
   }

   @OnlyIn(Dist.CLIENT)
   public static class State {
      private final ByteBuffer stateByteBuffer;
      private final VertexFormat stateVertexFormat;

      private State(ByteBuffer p_i225907_1_, VertexFormat p_i225907_2_) {
         this.stateByteBuffer = p_i225907_1_;
         this.stateVertexFormat = p_i225907_2_;
      }
   }
}
