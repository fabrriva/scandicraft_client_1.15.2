package net.minecraft.block;

import it.unimi.dsi.fastutil.objects.ObjectArraySet;
import java.util.Set;
import java.util.stream.Stream;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class WoodType {
   private static final Set<WoodType> VALUES = new ObjectArraySet<>();
   public static final WoodType OAK = func_227047_a_(new WoodType("oak"));
   public static final WoodType SPRUCE = func_227047_a_(new WoodType("spruce"));
   public static final WoodType BIRCH = func_227047_a_(new WoodType("birch"));
   public static final WoodType ACACIA = func_227047_a_(new WoodType("acacia"));
   public static final WoodType JUNGLE = func_227047_a_(new WoodType("jungle"));
   public static final WoodType DARK_OAK = func_227047_a_(new WoodType("dark_oak"));
   private final String name;

   protected WoodType(String p_i225775_1_) {
      this.name = p_i225775_1_;
   }

   private static WoodType func_227047_a_(WoodType p_227047_0_) {
      VALUES.add(p_227047_0_);
      return p_227047_0_;
   }

   @OnlyIn(Dist.CLIENT)
   public static Stream<WoodType> func_227046_a_() {
      return VALUES.stream();
   }

   @OnlyIn(Dist.CLIENT)
   public String func_227048_b_() {
      return this.name;
   }
}
