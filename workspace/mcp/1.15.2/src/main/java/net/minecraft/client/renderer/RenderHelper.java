package net.minecraft.client.renderer;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class RenderHelper {
   public static void func_227780_a_() {
      RenderSystem.enableLighting();
      RenderSystem.enableColorMaterial();
      RenderSystem.colorMaterial(1032, 5634);
   }

   public static void disableStandardItemLighting() {
      RenderSystem.disableLighting();
      RenderSystem.disableColorMaterial();
   }

   public static void setupLevelDiffuseLighting(Matrix4f p_227781_0_) {
      RenderSystem.setupLevelDiffuseLighting(p_227781_0_);
   }

   public static void setupGuiFlatDiffuseLighting() {
      RenderSystem.setupGuiFlatDiffuseLighting();
   }

   public static void setupGui3DDiffuseLighting() {
      RenderSystem.setupGui3DDiffuseLighting();
   }
}
