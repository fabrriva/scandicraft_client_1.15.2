REM vars
set gradle_mc="%USERPROFILE%\.gradle\caches\forge_gradle\minecraft_repo\versions\1.15.2"
set current_dir=%CD%

REM delete actual
del client-extra.jar

REM delete META_INF
del %current_dir%\jars\META-INF

REM archive jar
7z a client-extra.jar %current_dir%\jars\*

REM delete actual
del %gradle_mc%\client-extra.jar

REM copy new
copy client-extra.jar %gradle_mc%

PAUSE