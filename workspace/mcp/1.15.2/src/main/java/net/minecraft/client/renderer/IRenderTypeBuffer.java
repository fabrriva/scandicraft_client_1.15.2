package net.minecraft.client.renderer;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public interface IRenderTypeBuffer {
   static IRenderTypeBuffer.Impl getImpl(BufferBuilder p_228455_0_) {
      return getImpl(ImmutableMap.of(), p_228455_0_);
   }

   static IRenderTypeBuffer.Impl getImpl(Map<RenderType, BufferBuilder> p_228456_0_, BufferBuilder p_228456_1_) {
      return new IRenderTypeBuffer.Impl(p_228456_1_, p_228456_0_);
   }

   IVertexBuilder getBuffer(RenderType p_getBuffer_1_);

   @OnlyIn(Dist.CLIENT)
   public static class Impl implements IRenderTypeBuffer {
      protected final BufferBuilder defaultBuffer;
      protected final Map<RenderType, BufferBuilder> buffersByType;
      protected Optional<RenderType> lastRenderType = Optional.empty();
      protected final Set<BufferBuilder> startedBuffers = Sets.newHashSet();

      protected Impl(BufferBuilder p_i225969_1_, Map<RenderType, BufferBuilder> p_i225969_2_) {
         this.defaultBuffer = p_i225969_1_;
         this.buffersByType = p_i225969_2_;
      }

      public IVertexBuilder getBuffer(RenderType p_getBuffer_1_) {
         Optional<RenderType> optional = p_getBuffer_1_.func_230169_u_();
         BufferBuilder bufferbuilder = this.getOrDefault(p_getBuffer_1_);
         if (!Objects.equals(this.lastRenderType, optional)) {
            if (this.lastRenderType.isPresent()) {
               RenderType rendertype = this.lastRenderType.get();
               if (!this.buffersByType.containsKey(rendertype)) {
                  this.func_228462_a_(rendertype);
               }
            }

            if (this.startedBuffers.add(bufferbuilder)) {
               bufferbuilder.begin(p_getBuffer_1_.getGlMode(), p_getBuffer_1_.getVertexFormat());
            }

            this.lastRenderType = optional;
         }

         return bufferbuilder;
      }

      private BufferBuilder getOrDefault(RenderType p_228463_1_) {
         return this.buffersByType.getOrDefault(p_228463_1_, this.defaultBuffer);
      }

      public void func_228461_a_() {
         this.lastRenderType.ifPresent((p_228464_1_) -> {
            IVertexBuilder ivertexbuilder = this.getBuffer(p_228464_1_);
            if (ivertexbuilder == this.defaultBuffer) {
               this.func_228462_a_(p_228464_1_);
            }

         });

         for(RenderType rendertype : this.buffersByType.keySet()) {
            this.func_228462_a_(rendertype);
         }

      }

      public void func_228462_a_(RenderType p_228462_1_) {
         BufferBuilder bufferbuilder = this.getOrDefault(p_228462_1_);
         boolean flag = Objects.equals(this.lastRenderType, p_228462_1_.func_230169_u_());
         if (flag || bufferbuilder != this.defaultBuffer) {
            if (this.startedBuffers.remove(bufferbuilder)) {
               p_228462_1_.func_228631_a_(bufferbuilder, 0, 0, 0);
               if (flag) {
                  this.lastRenderType = Optional.empty();
               }

            }
         }
      }
   }
}
