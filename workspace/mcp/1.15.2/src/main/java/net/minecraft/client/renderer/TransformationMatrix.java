package net.minecraft.client.renderer;

import com.mojang.datafixers.util.Pair;
import java.util.Objects;
import javax.annotation.Nullable;
import net.minecraft.util.Util;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import org.apache.commons.lang3.tuple.Triple;

@OnlyIn(Dist.CLIENT)
public final class TransformationMatrix {
   private final Matrix4f matrix;
   private boolean decomposed;
   @Nullable
   private Vector3f translation;
   @Nullable
   private Quaternion rotationLeft;
   @Nullable
   private Vector3f scale;
   @Nullable
   private Quaternion rotationRight;
   private static final TransformationMatrix IDENTITY = Util.make(() -> {
      Matrix4f matrix4f = new Matrix4f();
      matrix4f.identity();
      TransformationMatrix transformationmatrix = new TransformationMatrix(matrix4f);
      transformationmatrix.func_227989_d_();
      return transformationmatrix;
   });

   public TransformationMatrix(@Nullable Matrix4f p_i225915_1_) {
      if (p_i225915_1_ == null) {
         this.matrix = IDENTITY.matrix;
      } else {
         this.matrix = p_i225915_1_;
      }

   }

   public TransformationMatrix(@Nullable Vector3f p_i225916_1_, @Nullable Quaternion p_i225916_2_, @Nullable Vector3f p_i225916_3_, @Nullable Quaternion p_i225916_4_) {
      this.matrix = func_227986_a_(p_i225916_1_, p_i225916_2_, p_i225916_3_, p_i225916_4_);
      this.translation = p_i225916_1_ != null ? p_i225916_1_ : new Vector3f();
      this.rotationLeft = p_i225916_2_ != null ? p_i225916_2_ : Quaternion.ONE.func_227068_g_();
      this.scale = p_i225916_3_ != null ? p_i225916_3_ : new Vector3f(1.0F, 1.0F, 1.0F);
      this.rotationRight = p_i225916_4_ != null ? p_i225916_4_ : Quaternion.ONE.func_227068_g_();
      this.decomposed = true;
   }

   public static TransformationMatrix func_227983_a_() {
      return IDENTITY;
   }

   public TransformationMatrix func_227985_a_(TransformationMatrix p_227985_1_) {
      Matrix4f matrix4f = this.func_227988_c_();
      matrix4f.multiply(p_227985_1_.func_227988_c_());
      return new TransformationMatrix(matrix4f);
   }

   @Nullable
   public TransformationMatrix func_227987_b_() {
      if (this == IDENTITY) {
         return this;
      } else {
         Matrix4f matrix4f = this.func_227988_c_();
         return matrix4f.func_226600_c_() ? new TransformationMatrix(matrix4f) : null;
      }
   }

   private void func_227990_e_() {
      if (!this.decomposed) {
         Pair<Matrix3f, Vector3f> pair = func_227984_a_(this.matrix);
         Triple<Quaternion, Vector3f, Quaternion> triple = pair.getFirst().func_226116_b_();
         this.translation = pair.getSecond();
         this.rotationLeft = triple.getLeft();
         this.scale = triple.getMiddle();
         this.rotationRight = triple.getRight();
         this.decomposed = true;
      }

   }

   private static Matrix4f func_227986_a_(@Nullable Vector3f p_227986_0_, @Nullable Quaternion p_227986_1_, @Nullable Vector3f p_227986_2_, @Nullable Quaternion p_227986_3_) {
      Matrix4f matrix4f = new Matrix4f();
      matrix4f.identity();
      if (p_227986_1_ != null) {
         matrix4f.multiply(new Matrix4f(p_227986_1_));
      }

      if (p_227986_2_ != null) {
         matrix4f.multiply(Matrix4f.func_226593_a_(p_227986_2_.getX(), p_227986_2_.getY(), p_227986_2_.getZ()));
      }

      if (p_227986_3_ != null) {
         matrix4f.multiply(new Matrix4f(p_227986_3_));
      }

      if (p_227986_0_ != null) {
         matrix4f.m30 = p_227986_0_.getX();
         matrix4f.m31 = p_227986_0_.getY();
         matrix4f.m32 = p_227986_0_.getZ();
      }

      return matrix4f;
   }

   public static Pair<Matrix3f, Vector3f> func_227984_a_(Matrix4f p_227984_0_) {
      p_227984_0_.func_226592_a_(1.0F / p_227984_0_.m33);
      Vector3f vector3f = new Vector3f(p_227984_0_.m30, p_227984_0_.m31, p_227984_0_.m32);
      Matrix3f matrix3f = new Matrix3f(p_227984_0_);
      return Pair.of(matrix3f, vector3f);
   }

   public Matrix4f func_227988_c_() {
      return this.matrix.copy();
   }

   public Quaternion func_227989_d_() {
      this.func_227990_e_();
      return this.rotationLeft.func_227068_g_();
   }

   public boolean equals(Object p_equals_1_) {
      if (this == p_equals_1_) {
         return true;
      } else if (p_equals_1_ != null && this.getClass() == p_equals_1_.getClass()) {
         TransformationMatrix transformationmatrix = (TransformationMatrix)p_equals_1_;
         return Objects.equals(this.matrix, transformationmatrix.matrix);
      } else {
         return false;
      }
   }

   public int hashCode() {
      return Objects.hash(this.matrix);
   }
}
