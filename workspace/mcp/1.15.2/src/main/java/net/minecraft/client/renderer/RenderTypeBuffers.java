package net.minecraft.client.renderer;

import it.unimi.dsi.fastutil.objects.Object2ObjectLinkedOpenHashMap;
import java.util.SortedMap;
import net.minecraft.client.renderer.model.ModelBakery;
import net.minecraft.util.Util;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class RenderTypeBuffers {
   private final RegionRenderCacheBuilder fixedBuilder = new RegionRenderCacheBuilder();
   private final SortedMap<RenderType, BufferBuilder> fixedBuffers = Util.make(new Object2ObjectLinkedOpenHashMap<>(), (p_228485_1_) -> {
      p_228485_1_.put(Atlases.func_228782_g_(), this.fixedBuilder.func_228366_a_(RenderType.solid()));
      p_228485_1_.put(Atlases.func_228783_h_(), this.fixedBuilder.func_228366_a_(RenderType.cutout()));
      p_228485_1_.put(Atlases.func_228768_a_(), this.fixedBuilder.func_228366_a_(RenderType.cutoutMipped()));
      p_228485_1_.put(Atlases.func_228784_i_(), this.fixedBuilder.func_228366_a_(RenderType.translucent()));
      func_228486_a_(p_228485_1_, Atlases.func_228776_b_());
      func_228486_a_(p_228485_1_, Atlases.func_228778_c_());
      func_228486_a_(p_228485_1_, Atlases.func_228779_d_());
      func_228486_a_(p_228485_1_, Atlases.func_228780_e_());
      func_228486_a_(p_228485_1_, Atlases.func_228781_f_());
      func_228486_a_(p_228485_1_, RenderType.translucentNoCrumbling());
      func_228486_a_(p_228485_1_, RenderType.glint());
      func_228486_a_(p_228485_1_, RenderType.entityGlint());
      func_228486_a_(p_228485_1_, RenderType.waterMask());
      ModelBakery.DESTROY_RENDER_TYPES.forEach((p_228488_1_) -> {
         func_228486_a_(p_228485_1_, p_228488_1_);
      });
   });
   private final IRenderTypeBuffer.Impl bufferSource = IRenderTypeBuffer.getImpl(this.fixedBuffers, new BufferBuilder(256));
   private final IRenderTypeBuffer.Impl crumblingBufferSource = IRenderTypeBuffer.getImpl(new BufferBuilder(256));
   private final OutlineLayerBuffer outlineBufferSource = new OutlineLayerBuffer(this.bufferSource);

   private static void func_228486_a_(Object2ObjectLinkedOpenHashMap<RenderType, BufferBuilder> p_228486_0_, RenderType p_228486_1_) {
      p_228486_0_.put(p_228486_1_, new BufferBuilder(p_228486_1_.defaultBufferSize()));
   }

   public RegionRenderCacheBuilder func_228484_a_() {
      return this.fixedBuilder;
   }

   public IRenderTypeBuffer.Impl func_228487_b_() {
      return this.bufferSource;
   }

   public IRenderTypeBuffer.Impl func_228489_c_() {
      return this.crumblingBufferSource;
   }

   public OutlineLayerBuffer func_228490_d_() {
      return this.outlineBufferSource;
   }
}
