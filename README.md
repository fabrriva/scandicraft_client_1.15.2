# scandicraft_client

## Setup
1. scandicraft_client/workspace/mcp/<mc_version>, Lancer:
.\gradlew setup
.\gradlew eclipse
.\gradlew genEclipseRuns
.\gradlew tasks

2. ouvrir workspace eclipse (scandicraft_client/workspace)
3. importer projet gradle depuis eclise (scandicraft_client/workspace/mcp/<mc_version>)
4. mettre les assets de la bonne version dans run/
5. launch client

## Edit assets (jar->json,png)
1. Copier le jar: run/copy_client-extra.jar.cmd
2. Modifier les assets du jar et SUPPRIMER META-INF\
3. Coller le jar: run/paste_client-extra.jar.cmd