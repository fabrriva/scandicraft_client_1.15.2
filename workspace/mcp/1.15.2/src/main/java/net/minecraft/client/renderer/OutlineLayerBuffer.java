package net.minecraft.client.renderer;

import com.mojang.blaze3d.vertex.DefaultColorVertexBuilder;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.vertex.VertexBuilderUtils;
import java.util.Optional;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class OutlineLayerBuffer implements IRenderTypeBuffer {
   private final IRenderTypeBuffer.Impl buffer;
   private final IRenderTypeBuffer.Impl outlineBuffer = IRenderTypeBuffer.getImpl(new BufferBuilder(256));
   private int red = 255;
   private int green = 255;
   private int blue = 255;
   private int alpha = 255;

   public OutlineLayerBuffer(IRenderTypeBuffer.Impl p_i225970_1_) {
      this.buffer = p_i225970_1_;
   }

   public IVertexBuilder getBuffer(RenderType p_getBuffer_1_) {
      if (p_getBuffer_1_.func_230041_s_()) {
         IVertexBuilder ivertexbuilder2 = this.outlineBuffer.getBuffer(p_getBuffer_1_);
         return new OutlineLayerBuffer.ColoredOutline(ivertexbuilder2, this.red, this.green, this.blue, this.alpha);
      } else {
         IVertexBuilder ivertexbuilder = this.buffer.getBuffer(p_getBuffer_1_);
         Optional<RenderType> optional = p_getBuffer_1_.func_225612_r_();
         if (optional.isPresent()) {
            IVertexBuilder ivertexbuilder1 = this.outlineBuffer.getBuffer(optional.get());
            OutlineLayerBuffer.ColoredOutline outlinelayerbuffer$coloredoutline = new OutlineLayerBuffer.ColoredOutline(ivertexbuilder1, this.red, this.green, this.blue, this.alpha);
            return VertexBuilderUtils.newDelegate(outlinelayerbuffer$coloredoutline, ivertexbuilder);
         } else {
            return ivertexbuilder;
         }
      }
   }

   public void func_228472_a_(int p_228472_1_, int p_228472_2_, int p_228472_3_, int p_228472_4_) {
      this.red = p_228472_1_;
      this.green = p_228472_2_;
      this.blue = p_228472_3_;
      this.alpha = p_228472_4_;
   }

   public void func_228471_a_() {
      this.outlineBuffer.func_228461_a_();
   }

   @OnlyIn(Dist.CLIENT)
   static class ColoredOutline extends DefaultColorVertexBuilder {
      private final IVertexBuilder coloredBuffer;
      private double x;
      private double y;
      private double z;
      private float u;
      private float v;

      private ColoredOutline(IVertexBuilder p_i225971_1_, int p_i225971_2_, int p_i225971_3_, int p_i225971_4_, int p_i225971_5_) {
         this.coloredBuffer = p_i225971_1_;
         super.func_225611_b_(p_i225971_2_, p_i225971_3_, p_i225971_4_, p_i225971_5_);
      }

      public void func_225611_b_(int p_225611_1_, int p_225611_2_, int p_225611_3_, int p_225611_4_) {
      }

      public IVertexBuilder pos(double p_225582_1_, double p_225582_3_, double p_225582_5_) {
         this.x = p_225582_1_;
         this.y = p_225582_3_;
         this.z = p_225582_5_;
         return this;
      }

      public IVertexBuilder color(int p_225586_1_, int p_225586_2_, int p_225586_3_, int p_225586_4_) {
         return this;
      }

      public IVertexBuilder tex(float p_225583_1_, float p_225583_2_) {
         this.u = p_225583_1_;
         this.v = p_225583_2_;
         return this;
      }

      public IVertexBuilder func_225585_a_(int p_225585_1_, int p_225585_2_) {
         return this;
      }

      public IVertexBuilder lightmap(int p_225587_1_, int p_225587_2_) {
         return this;
      }

      public IVertexBuilder func_225584_a_(float p_225584_1_, float p_225584_2_, float p_225584_3_) {
         return this;
      }

      public void func_225588_a_(float p_225588_1_, float p_225588_2_, float p_225588_3_, float red, float green, float blue, float alpha, float p_225588_8_, float p_225588_9_, int p_225588_10_, int p_225588_11_, float p_225588_12_, float p_225588_13_, float p_225588_14_) {
         this.coloredBuffer.pos((double)p_225588_1_, (double)p_225588_2_, (double)p_225588_3_).color(this.defaultRed, this.defaultGreen, this.defaultBlue, this.defaultAlpha).tex(p_225588_8_, p_225588_9_).endVertex();
      }

      public void endVertex() {
         this.coloredBuffer.pos(this.x, this.y, this.z).color(this.defaultRed, this.defaultGreen, this.defaultBlue, this.defaultAlpha).tex(this.u, this.v).endVertex();
      }
   }
}
