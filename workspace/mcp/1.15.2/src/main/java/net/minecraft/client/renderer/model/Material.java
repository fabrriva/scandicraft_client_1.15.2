package net.minecraft.client.renderer.model;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import java.util.Objects;
import java.util.function.Function;
import javax.annotation.Nullable;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class Material {
   private final ResourceLocation atlasLocation;
   private final ResourceLocation textureLocation;
   @Nullable
   private RenderType renderType;

   public Material(ResourceLocation p_i226055_1_, ResourceLocation p_i226055_2_) {
      this.atlasLocation = p_i226055_1_;
      this.textureLocation = p_i226055_2_;
   }

   public ResourceLocation func_229310_a_() {
      return this.atlasLocation;
   }

   public ResourceLocation func_229313_b_() {
      return this.textureLocation;
   }

   public TextureAtlasSprite getSprite() {
      return Minecraft.getInstance().getTextureGetter(this.func_229310_a_()).apply(this.func_229313_b_());
   }

   public RenderType func_229312_a_(Function<ResourceLocation, RenderType> p_229312_1_) {
      if (this.renderType == null) {
         this.renderType = p_229312_1_.apply(this.atlasLocation);
      }

      return this.renderType;
   }

   public IVertexBuilder func_229311_a_(IRenderTypeBuffer p_229311_1_, Function<ResourceLocation, RenderType> p_229311_2_) {
      return this.getSprite().func_229230_a_(p_229311_1_.getBuffer(this.func_229312_a_(p_229311_2_)));
   }

   public boolean equals(Object p_equals_1_) {
      if (this == p_equals_1_) {
         return true;
      } else if (p_equals_1_ != null && this.getClass() == p_equals_1_.getClass()) {
         Material material = (Material)p_equals_1_;
         return this.atlasLocation.equals(material.atlasLocation) && this.textureLocation.equals(material.textureLocation);
      } else {
         return false;
      }
   }

   public int hashCode() {
      return Objects.hash(this.atlasLocation, this.textureLocation);
   }

   public String toString() {
      return "Material{atlasLocation=" + this.atlasLocation + ", texture=" + this.textureLocation + '}';
   }
}
