package fr.scandicraft.items;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;

public class RepairScepter extends Item {

	public RepairScepter(Item.Properties builder, int durabilityIN) {
		super(builder);
	}

	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		repair(playerIn.inventory.mainInventory);
		repair(playerIn.inventory.armorInventory);

		return ActionResult.func_226248_a_(playerIn.getHeldItemMainhand());
	}

	private void repair(NonNullList<ItemStack> items) {
		for (int i = 0; i < items.size(); i++) {
			items.get(i).setDamage(0);
		}
	}

}
