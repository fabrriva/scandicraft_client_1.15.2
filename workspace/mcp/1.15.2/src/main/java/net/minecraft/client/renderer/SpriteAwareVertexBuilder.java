package net.minecraft.client.renderer;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class SpriteAwareVertexBuilder implements IVertexBuilder {
   private final IVertexBuilder vertexBuilder;
   private final TextureAtlasSprite atlasSprite;

   public SpriteAwareVertexBuilder(IVertexBuilder p_i225999_1_, TextureAtlasSprite p_i225999_2_) {
      this.vertexBuilder = p_i225999_1_;
      this.atlasSprite = p_i225999_2_;
   }

   public IVertexBuilder pos(double p_225582_1_, double p_225582_3_, double p_225582_5_) {
      return this.vertexBuilder.pos(p_225582_1_, p_225582_3_, p_225582_5_);
   }

   public IVertexBuilder color(int p_225586_1_, int p_225586_2_, int p_225586_3_, int p_225586_4_) {
      return this.vertexBuilder.color(p_225586_1_, p_225586_2_, p_225586_3_, p_225586_4_);
   }

   public IVertexBuilder tex(float p_225583_1_, float p_225583_2_) {
      return this.vertexBuilder.tex(this.atlasSprite.getInterpolatedU((double)(p_225583_1_ * 16.0F)), this.atlasSprite.getInterpolatedV((double)(p_225583_2_ * 16.0F)));
   }

   public IVertexBuilder func_225585_a_(int p_225585_1_, int p_225585_2_) {
      return this.vertexBuilder.func_225585_a_(p_225585_1_, p_225585_2_);
   }

   public IVertexBuilder lightmap(int p_225587_1_, int p_225587_2_) {
      return this.vertexBuilder.lightmap(p_225587_1_, p_225587_2_);
   }

   public IVertexBuilder func_225584_a_(float p_225584_1_, float p_225584_2_, float p_225584_3_) {
      return this.vertexBuilder.func_225584_a_(p_225584_1_, p_225584_2_, p_225584_3_);
   }

   public void endVertex() {
      this.vertexBuilder.endVertex();
   }

   public void func_225588_a_(float p_225588_1_, float p_225588_2_, float p_225588_3_, float red, float green, float blue, float alpha, float p_225588_8_, float p_225588_9_, int p_225588_10_, int p_225588_11_, float p_225588_12_, float p_225588_13_, float p_225588_14_) {
      this.vertexBuilder.func_225588_a_(p_225588_1_, p_225588_2_, p_225588_3_, red, green, blue, alpha, this.atlasSprite.getInterpolatedU((double)(p_225588_8_ * 16.0F)), this.atlasSprite.getInterpolatedV((double)(p_225588_9_ * 16.0F)), p_225588_10_, p_225588_11_, p_225588_12_, p_225588_13_, p_225588_14_);
   }
}
