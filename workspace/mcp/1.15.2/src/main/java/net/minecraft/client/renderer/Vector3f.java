package net.minecraft.client.renderer;

import it.unimi.dsi.fastutil.floats.Float2FloatFunction;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public final class Vector3f {
   public static Vector3f field_229178_a_ = new Vector3f(-1.0F, 0.0F, 0.0F);
   public static Vector3f field_229179_b_ = new Vector3f(1.0F, 0.0F, 0.0F);
   public static Vector3f field_229180_c_ = new Vector3f(0.0F, -1.0F, 0.0F);
   public static Vector3f field_229181_d_ = new Vector3f(0.0F, 1.0F, 0.0F);
   public static Vector3f field_229182_e_ = new Vector3f(0.0F, 0.0F, -1.0F);
   public static Vector3f field_229183_f_ = new Vector3f(0.0F, 0.0F, 1.0F);
   private float x;
   private float y;
   private float z;

   public Vector3f() {
   }

   public Vector3f(float x, float y, float z) {
      this.x = x;
      this.y = y;
      this.z = z;
   }

   public Vector3f(Vec3d p_i51412_1_) {
      this((float)p_i51412_1_.x, (float)p_i51412_1_.y, (float)p_i51412_1_.z);
   }

   public boolean equals(Object p_equals_1_) {
      if (this == p_equals_1_) {
         return true;
      } else if (p_equals_1_ != null && this.getClass() == p_equals_1_.getClass()) {
         Vector3f vector3f = (Vector3f)p_equals_1_;
         if (Float.compare(vector3f.x, this.x) != 0) {
            return false;
         } else if (Float.compare(vector3f.y, this.y) != 0) {
            return false;
         } else {
            return Float.compare(vector3f.z, this.z) == 0;
         }
      } else {
         return false;
      }
   }

   public int hashCode() {
      int i = Float.floatToIntBits(this.x);
      i = 31 * i + Float.floatToIntBits(this.y);
      i = 31 * i + Float.floatToIntBits(this.z);
      return i;
   }

   public float getX() {
      return this.x;
   }

   public float getY() {
      return this.y;
   }

   public float getZ() {
      return this.z;
   }

   @OnlyIn(Dist.CLIENT)
   public void mul(float multiplier) {
      this.x *= multiplier;
      this.y *= multiplier;
      this.z *= multiplier;
   }

   @OnlyIn(Dist.CLIENT)
   public void mul(float p_229192_1_, float p_229192_2_, float p_229192_3_) {
      this.x *= p_229192_1_;
      this.y *= p_229192_2_;
      this.z *= p_229192_3_;
   }

   @OnlyIn(Dist.CLIENT)
   public void clamp(float min, float max) {
      this.x = MathHelper.clamp(this.x, min, max);
      this.y = MathHelper.clamp(this.y, min, max);
      this.z = MathHelper.clamp(this.z, min, max);
   }

   public void set(float x, float y, float z) {
      this.x = x;
      this.y = y;
      this.z = z;
   }

   @OnlyIn(Dist.CLIENT)
   public void add(float x, float y, float z) {
      this.x += x;
      this.y += y;
      this.z += z;
   }

   @OnlyIn(Dist.CLIENT)
   public void add(Vector3f p_229189_1_) {
      this.x += p_229189_1_.x;
      this.y += p_229189_1_.y;
      this.z += p_229189_1_.z;
   }

   @OnlyIn(Dist.CLIENT)
   public void sub(Vector3f vec) {
      this.x -= vec.x;
      this.y -= vec.y;
      this.z -= vec.z;
   }

   @OnlyIn(Dist.CLIENT)
   public float dot(Vector3f vec) {
      return this.x * vec.x + this.y * vec.y + this.z * vec.z;
   }

   @OnlyIn(Dist.CLIENT)
   public boolean func_229194_d_() {
      float f = this.x * this.x + this.y * this.y + this.z * this.z;
      if ((double)f < 1.0E-5D) {
         return false;
      } else {
         float f1 = MathHelper.func_226165_i_(f);
         this.x *= f1;
         this.y *= f1;
         this.z *= f1;
         return true;
      }
   }

   @OnlyIn(Dist.CLIENT)
   public void cross(Vector3f vec) {
      float f = this.x;
      float f1 = this.y;
      float f2 = this.z;
      float f3 = vec.getX();
      float f4 = vec.getY();
      float f5 = vec.getZ();
      this.x = f1 * f5 - f2 * f4;
      this.y = f2 * f3 - f * f5;
      this.z = f * f4 - f1 * f3;
   }

   @OnlyIn(Dist.CLIENT)
   public void func_229188_a_(Matrix3f p_229188_1_) {
      float f = this.x;
      float f1 = this.y;
      float f2 = this.z;
      this.x = p_229188_1_.m00 * f + p_229188_1_.m10 * f1 + p_229188_1_.m20 * f2;
      this.y = p_229188_1_.m01 * f + p_229188_1_.m11 * f1 + p_229188_1_.m21 * f2;
      this.z = p_229188_1_.m02 * f + p_229188_1_.m12 * f1 + p_229188_1_.m22 * f2;
   }

   public void func_214905_a(Quaternion p_214905_1_) {
      Quaternion quaternion = new Quaternion(p_214905_1_);
      quaternion.multiply(new Quaternion(this.getX(), this.getY(), this.getZ(), 0.0F));
      Quaternion quaternion1 = new Quaternion(p_214905_1_);
      quaternion1.conjugate();
      quaternion.multiply(quaternion1);
      this.set(quaternion.getX(), quaternion.getY(), quaternion.getZ());
   }

   @OnlyIn(Dist.CLIENT)
   public void func_229190_a_(Vector3f p_229190_1_, float p_229190_2_) {
      float f = 1.0F - p_229190_2_;
      this.x = this.x * f + p_229190_1_.x * p_229190_2_;
      this.y = this.y * f + p_229190_1_.y * p_229190_2_;
      this.z = this.z * f + p_229190_1_.z * p_229190_2_;
   }

   @OnlyIn(Dist.CLIENT)
   public Quaternion func_229193_c_(float p_229193_1_) {
      return new Quaternion(this, p_229193_1_, false);
   }

   @OnlyIn(Dist.CLIENT)
   public Quaternion func_229187_a_(float p_229187_1_) {
      return new Quaternion(this, p_229187_1_, true);
   }

   @OnlyIn(Dist.CLIENT)
   public Vector3f copy() {
      return new Vector3f(this.x, this.y, this.z);
   }

   @OnlyIn(Dist.CLIENT)
   public void func_229191_a_(Float2FloatFunction p_229191_1_) {
      this.x = p_229191_1_.get(this.x);
      this.y = p_229191_1_.get(this.y);
      this.z = p_229191_1_.get(this.z);
   }

   public String toString() {
      return "[" + this.x + ", " + this.y + ", " + this.z + "]";
   }
}
