package net.minecraft.client.renderer;

import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class Vector4f {
   private float x;
   private float y;
   private float z;
   private float w;

   public Vector4f() {
   }

   public Vector4f(float x, float y, float z, float w) {
      this.x = x;
      this.y = y;
      this.z = z;
      this.w = w;
   }

   public Vector4f(Vector3f p_i226061_1_) {
      this(p_i226061_1_.getX(), p_i226061_1_.getY(), p_i226061_1_.getZ(), 1.0F);
   }

   public boolean equals(Object p_equals_1_) {
      if (this == p_equals_1_) {
         return true;
      } else if (p_equals_1_ != null && this.getClass() == p_equals_1_.getClass()) {
         Vector4f vector4f = (Vector4f)p_equals_1_;
         if (Float.compare(vector4f.x, this.x) != 0) {
            return false;
         } else if (Float.compare(vector4f.y, this.y) != 0) {
            return false;
         } else if (Float.compare(vector4f.z, this.z) != 0) {
            return false;
         } else {
            return Float.compare(vector4f.w, this.w) == 0;
         }
      } else {
         return false;
      }
   }

   public int hashCode() {
      int i = Float.floatToIntBits(this.x);
      i = 31 * i + Float.floatToIntBits(this.y);
      i = 31 * i + Float.floatToIntBits(this.z);
      i = 31 * i + Float.floatToIntBits(this.w);
      return i;
   }

   public float getX() {
      return this.x;
   }

   public float getY() {
      return this.y;
   }

   public float getZ() {
      return this.z;
   }

   public float getW() {
      return this.w;
   }

   public void scale(Vector3f vec) {
      this.x *= vec.getX();
      this.y *= vec.getY();
      this.z *= vec.getZ();
   }

   public void set(float x, float y, float z, float w) {
      this.x = x;
      this.y = y;
      this.z = z;
      this.w = w;
   }

   public float func_229373_a_(Vector4f p_229373_1_) {
      return this.x * p_229373_1_.x + this.y * p_229373_1_.y + this.z * p_229373_1_.z + this.w * p_229373_1_.w;
   }

   public boolean func_229374_e_() {
      float f = this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w;
      if ((double)f < 1.0E-5D) {
         return false;
      } else {
         float f1 = MathHelper.func_226165_i_(f);
         this.x *= f1;
         this.y *= f1;
         this.z *= f1;
         this.w *= f1;
         return true;
      }
   }

   public void func_229372_a_(Matrix4f p_229372_1_) {
      float f = this.x;
      float f1 = this.y;
      float f2 = this.z;
      float f3 = this.w;
      this.x = p_229372_1_.m00 * f + p_229372_1_.m10 * f1 + p_229372_1_.m20 * f2 + p_229372_1_.m30 * f3;
      this.y = p_229372_1_.m01 * f + p_229372_1_.m11 * f1 + p_229372_1_.m21 * f2 + p_229372_1_.m31 * f3;
      this.z = p_229372_1_.m02 * f + p_229372_1_.m12 * f1 + p_229372_1_.m22 * f2 + p_229372_1_.m32 * f3;
      this.w = p_229372_1_.m03 * f + p_229372_1_.m13 * f1 + p_229372_1_.m23 * f2 + p_229372_1_.m33 * f3;
   }

   public void func_195912_a(Quaternion quaternionIn) {
      Quaternion quaternion = new Quaternion(quaternionIn);
      quaternion.multiply(new Quaternion(this.getX(), this.getY(), this.getZ(), 0.0F));
      Quaternion quaternion1 = new Quaternion(quaternionIn);
      quaternion1.conjugate();
      quaternion.multiply(quaternion1);
      this.set(quaternion.getX(), quaternion.getY(), quaternion.getZ(), this.getW());
   }

   public void func_229375_f_() {
      this.x /= this.w;
      this.y /= this.w;
      this.z /= this.w;
      this.w = 1.0F;
   }

   public String toString() {
      return "[" + this.x + ", " + this.y + ", " + this.z + ", " + this.w + "]";
   }
}
